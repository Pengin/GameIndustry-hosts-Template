<h1 align="center">GameIndustry host Templates</h1>
<h3 align="center">Unique host templates to enhance own privacy in games, websites and regulary software</h3>

### No longer maintained
Since there hasn't been an update to the lists on Codeberg for a while now, here's the clear indication that they are no longer maintained on Codeberg. For the most current data and blocklists, please visit either the project page at <a href="https://gameindustry.eu">GameIndustry.eu</a> or the <a href="https://github.com/KodoPengin/GameIndustry-hosts-Template/">Github repository</a>.

Thank you very much and apologize for any inconvenience.
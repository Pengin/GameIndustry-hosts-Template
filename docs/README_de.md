<h1 align="center">GameIndustry-Hostvorlagen</h1>
<h3 align="center">Einzigartige Hostvorlagen zur Verbesserung der eigenen Privatsphäre in Spielen, Webseiten und regulärer Software</h3>

### Nicht länger gepflegt
Da es nun auf Codeberg schon länger kein Update der Listen gab, nun der offensichtliche Hinweis, dass diese auf der Plattform nicht länger gepflegt werden. Für die aktuellsten Daten und Blocklisten besucht bitte entweder die Projektseite unter <a href="https://gameindustry.eu">GameIndustry.eu</a> oder das <a href="https://github.com/KodoPengin/GameIndustry-hosts-Template/">Github-Repository</a>.

Vielen Dank und entschuldigt die Unannehmlichkeiten.